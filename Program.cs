﻿using System;
using Gtk;
using Action = System.Action;

namespace Minesweeper {
    public class Program {
        private static readonly Window Window = new("Minesweeper - Main Menu");
        private static readonly VBox Root = new(); 
        
        private static readonly ScaleBar WidthBox  = new ScaleBar("Grid Width", 5, 20);
        private static readonly ScaleBar HeightBox = new ScaleBar("Grid Height", 5, 20);
        private static readonly ScaleBar MineBox   = new ScaleBar("Mine Frequency", 0, 100);
        private static bool _startGameFlag = false;
        
        public static void Main(string[] args) {
            Application.Init();
            Root.Spacing = 10;
            Root.Add(WidthBox);
            Root.Add(HeightBox);
            Root.Add(MineBox);
            MineBox.Bar.Value = 33;

            MineBox.Bar.TooltipText = "Approximate percentage of mines on the grid. Will not be exact.";
            
            Button playButton = new();
            playButton.Label = "Start Game";

            Window.DeleteEvent += (o, eventArgs) => {
                if (!_startGameFlag) {
                    Application.Quit();
                }
            };

            playButton.ButtonReleaseEvent += (o, eventArgs) => {
                _startGameFlag = true;
                int tileCount = (int) (WidthBox.GetValue() * HeightBox.GetValue());
                Game.Init(
                    WidthBox.GetValue(),
                    HeightBox.GetValue(),
                    (int) ((MineBox.GetValue()/100f)*tileCount)
                );
                Window.Destroy();
            }; 
            
            Root.Add(playButton);
            
            Window.Add(Root);
            Window.ShowAll();

            Application.Run();
        }

        class ScaleBar : HBox {
            public readonly HScale Bar;
            public uint GetValue() {
                return (uint) Bar.Value;
            }

            public ScaleBar(string text, int min, int max) {
                Bar = new HScale(min, max, 1);
                Bar.ValueChanged += (_, _) => {};
                Label label = new Label(text);
                label.Halign = Align.Start;
                Bar.Halign = Align.End;
                Bar.WidthRequest = 120;
                Add(label);
                Add(Bar);
            }
        }
    }
}