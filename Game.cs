using System;
using System.Collections;
using Gdk;
using Gtk;
using Window = Gtk.Window;

namespace Minesweeper {
    public class Game {
        private static readonly ArrayList MineArrays = new();
        private static readonly Window Root = new("Minesweeper");
        private static int _mineCount;
        private static readonly Random RNG = new();
        private static readonly Label StateLabel = new();
        private static int _amountClear;
        private static int _realMineCount;
        private static int _amountOpen;
        private static int _tileCount;
        private static Color _mineBg;
        public static void Init(uint w, uint h, int mineCount) {
            Color.Parse("#660000", ref _mineBg);
            
            _mineCount = mineCount;
            _tileCount = (int)(w * h);
            
            VBox frame = new();
            frame.Spacing = 10;

            HBox header = new();
            
            Button resetButton = new();
            resetButton.Label  = "Restart";
            resetButton.Expand = false;
            resetButton.Halign = Align.End;
            resetButton.Clicked += HandleResetButton;

            StateLabel.Expand = false;
            StateLabel.Halign = Align.Start;
            
            Table gridTable = new(h, w, true);

            for (uint i = 0; i < w; i++) {
                MineArrays.Add(new ArrayList());
                for (uint j = 0; j < h; j++) {
                    MineButton mineButton = new((int)(j), (int)(i));
                    ((ArrayList) MineArrays[^1])!.Add(mineButton);
                    gridTable.Attach(mineButton, i, i+1, j, j+1);
                }
            }
            
            Root.Add(frame);
            header.Add(StateLabel);
            header.Add(resetButton);
            frame.Add(header);
            frame.Add(gridTable);

            Root.DeleteEvent += (_, _) => { Application.Quit(); };
            
            PopulateGrid();
            
            Root.ShowAll();
        }

        private static void FinishGame(bool state) {
            foreach (ArrayList i in MineArrays) {
                foreach (MineButton b in i) {
                    b.Reveal();
                }
            }
            StateLabel.Text = state? "You Won!" : "You Lost!";
        }
        
        private static void HandleResetButton(object obj, EventArgs args) {
            StateLabel.Text = "";
            ClearGrid();
            PopulateGrid();
        }
        
        private static void PopulateGrid() {
            int maxRow = ((ArrayList) MineArrays[0])!.Count - 1;
            int maxCol = MineArrays.Count - 1;
            for (int i = 0; i < _mineCount; i++) {
                int row = RNG.Next(maxRow);
                int col = RNG.Next(maxCol);
                
                ((MineButton) ((ArrayList) MineArrays[col])![row])!.IsMine = true;
                ((MineButton) ((ArrayList) MineArrays[col])![row])!.Number = 0;
                _realMineCount++;
            }
            
            // Jank but works
            // Oh well
            for (int col = 0; col <= maxCol; col++) {
                for (int row = 0; row <= maxRow; row++) {

                    MineButton tile = ((MineButton) ((ArrayList) MineArrays[col])![row])!;
                    if (tile!.IsMine) continue;
                    for (int x = ((col>0)? col-1 : col); x <= ((col<maxCol)? col+1 : col); x++) {
                        for (int y = ((row>0)? row-1 : row); y <= ((row<maxRow)? row+1 : row); y++) {
                            if (x == col && y == row) continue;
                            if (((MineButton) ((ArrayList) MineArrays[x])![y])!.IsMine) {
                                tile!.IncNumber();
                            }
                        }
                    }
                }
            }
        }

        private static void ClearGrid() {
            foreach (ArrayList i in MineArrays) {
                foreach (MineButton b in i) {
                    b.Clear();
                    b.Sensitive = true;
                    b.ModifyBg(StateType.Insensitive);
                }
            }

            _realMineCount = 0;
            _amountClear = 0;
            _amountOpen = 0;
        }

        private static ArrayList ActivateAdjacentNumberTiles(int row, int col, ArrayList visited) {
            int maxRow = ((ArrayList) MineArrays[0])!.Count - 1;
            int maxCol = MineArrays.Count - 1;

            for (int x = ((col>0)? col-1 : col); x <= ((col<maxCol)? col+1 : col); x++) {
                for (int y = ((row>0)? row-1 : row); y <= ((row<maxRow)? row+1 : row); y++) {
                    MineButton button = (MineButton)((ArrayList) MineArrays[x])![y];
                    
                    if (!button!.IsMine) {
                        button!.Reveal();
                    }

                    if (button.Number == 0 && !button!.IsMine) {
                        if (!visited.Contains(button)) {
                            visited.Add(button);
                            visited = ActivateAdjacentNumberTiles(button.Row, button.Col, visited);
                        }
                    }
                }
            }

            return visited;
        }
            
        class MineButton : Button {
            public int Number;
            public bool IsMine;
            private bool _isFlagged;
            public int Row { get; }
            public int Col { get; }
            private bool _isRevealed;
            
            public MineButton(int row, int col) {
                ButtonReleaseEvent += Activate;
                Label = " ";
                Row = row;
                Col = col;
            }

            private void ToggleFlag() {
                _isFlagged = !_isFlagged;
                Label = _isFlagged? "P" : " ";
                _amountClear += IsMine? _isFlagged? 1 : -1 : 0;
            }
        
            public void Clear() {
                IsMine = false;
                _isFlagged = false;
                Relief = ReliefStyle.Normal;
                Number = 0;
                Label  = " ";
                _isRevealed = false;
            }

            public void Reveal() {
                if (_isRevealed) { return; }
                _isRevealed = true;
                Relief = ReliefStyle.Normal;
                Label = IsMine? "¤" : Number.ToString();
                Sensitive = false;
                _amountOpen++;
            }

            public void IncNumber() {
                Number++;
            }

            private void Activate(object obj, ButtonReleaseEventArgs args) {
                if (args.Event.Button == 3) {
                    ToggleFlag();
                }

                if (args.Event.Button == 1 && !_isFlagged) {
                    Relief = ReliefStyle.None;
                    Sensitive = false;
                    Reveal();
                    if (IsMine) {
                        ModifyBg(StateType.Insensitive, _mineBg);
                    }
                    if (IsMine) { FinishGame(false); return; }
                    if (Number == 0) { ActivateAdjacentNumberTiles(Row, Col, new ArrayList()); }
                }
                
                if (_amountClear == _realMineCount && _amountOpen == (_tileCount-_realMineCount)) {
                    FinishGame(true);
                }
            }
        }
    }
}